[1] Firefox Implements in browser DNS "safety" measures. Firefox will start bypassing internal (Operating System) DNS servers utilizing CloudFlare DNS services instead. This can be disabled in about:config and Group Policy settings. This new DNS service utilizes DNS over HTTPS (DOH) and could cause you grief within your network when attempting to access local resources. This is a great consumer benefit but does not do any justice for corporate/enterprise networks.

[2] Microsoft - Remote Code Execution - 19+ Critical
Severity: High
Overview: Multiple vulnerabilities in Microsoft Products (Windows, Windows Server, Office, IE, Edge, SQL, Exchange, .NET Framework) allows for remote code execution. Attackers gain the same privilege as logged on users and depending on user rights could traverse your internal network.

Recommendations:
- Setup a Windows Server Update Service (WSUS) server for accountability and
    ability to deny updates due to poor quality control.
- Apply pertinent patches
- Restrict your accounts using the principle of least privilege (IE: Don't login to any desktop as a domain admin)

[3] Adobe - Arbitrary Code Execution
Severity: Moderate
Overview: A successful attack in the form of a malformed PDF or Flash player could result in an attacker gaining control of your desktop.

Systems Affected: Microsoft Windows, Mac OS, Linux.

Recommendations:
- Apply patches from Adobe (this includes Creative "cloud")
- Restrict your accounts using the principle of least privilege (IE: Don't login to your desktop as a domain admin)

[4] G-Suite Security Updates - G-Suite can now alert you of government-backed attacks. Google will now alert admins if a Government or Nation State attack is directed to your G-Suite domain. To enable go to admin.google.com -> reports ->  Manage Alerts -> Government backed attack

[5] HP Printers - Remote Code Execution
Severity: High for Internet connected printers
Overview: Multiple Vulnerabilities have been discovered in HP Printer products, which could allow for remote code execution. Depending on the printer’s placement on the network, an attacker could potentially install programs; view, change, or delete data; or create new accounts with full user rights.

Recommendations:
- Apply appropriate updates provided by HP.
- Change all Default printer login credentials.
- Disable unnecessary functions.


[6] VMware - Multiple vulnerabilities and CVE
Severity: Moderate
Overview: Several vulnerabilities have been discovered. Too much data to list, link below. Patches are pending.

Recommendations:
- Apply appropriate updates provided by VMware when released.

[7] WPA3
Overview: The Wi-Fi alliance, which is largely a closed communication and closed source of information, has released some information regarding the much-needed upgrade to WPA2. The sparse details that the wi-fi alliance has released include:

- Simultaneous Authentication of Equals (Replaces PSK) to combat offline dictionary attacks
- Forward secrecy - protects data even if password was compromised.
- Tons of encryption algorithm updates
- Enhancements for IOT stuff


[8] OpenSSH
Severity: Moderate
Overview: Using a dictionary an attacker could gain access to all usernames on a given system. Proof of concept is already in the wild.

Recommendations: 
 - Update to openssh version 1:7.7p1-1 or greater.
 - If you have a good backup scheme, I would set to auto update. 
 

[9] HP Fax Vulnerability
Severity: Hmm. Who uses fax machines anymore?
Overview: Great proof of concept video in the links below. Attackers can send a properly formatted fax to a remote fax machine, once the payload is received by the remote fax machine, the remote fax machine is then used to infiltrate the network using the NSA exploit EternalBlue. Once data is found, the data is faxed back to the attacker.

Recommendations: Update HP Firmware

- Exercise caution when using an IoT device on an unprotected VLAN (ie, don't do it!)
- Utilize principle of least privilege*.

[10] Windows Zero-Day
Severity: needs local access: Moderate
Overview: Local privilege escalation in the Advanced Local Procedure Call
(ALPC) to gain system privileges. 

Recommendations: 

- Bleeding edge: https://0patch.com/
- Non bleeding-edge: Wait for Microsoft to patch

---

Links:
[1] Firefox DOH: https://blog.nightly.mozilla.org/2018/06/01/improving-dns-privacy-in-firefox/  
[2] Microsoft: https://portal.msrc.microsoft.com/en-us/security-guidance/summary  
[3] Adobe:  https://securityboulevard.com/2018/08/apsb18-29-adobe-releases-august-2018-security-updates/  
[4] G-Suite: https://www.bleepingcomputer.com/news/security/g-suite-can-now-alert-you-of-government-backed-attacks/  
[5] HP Printers: https://support.hp.com/us-en/document/c06097712  
[6] VMware: https://www.vmware.com/security/advisories.html  
[7] WPA3: https://www.howtogeek.com/339765/what-is-wpa3-and-when-will-i-get-it-on-my-wi-fi/  
[8] https://www.bleepingcomputer.com/news/security/vulnerability-affects-all-openssh-versions-released-in-the-past-two-decades/  
[9] https://research.checkpoint.com/sending-fax-back-to-the-dark-ages/#qa  
https://support.hp.com/us-en/document/c06097712  
[10] https://www.bleepingcomputer.com/news/security/temporary-patch-available-for-recent-windows-task-scheduler-alpc-zero-day/  

* https://en.wikipedia.org/wiki/Principle_of_least_privilege
